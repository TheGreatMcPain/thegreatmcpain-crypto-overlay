# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cuda cmake toolchain-funcs flag-o-matic

DESCRIPTION="CUDA Plugin for xmrig"
HOMEPAGE="https://github.com/xmrig/xmrig-cuda"
SRC_URI="https://github.com/xmrig/xmrig-cuda/archive/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64"

LICENSE="GPL-3"
SLOT="0"

DEPEND="
	dev-util/nvidia-cuda-toolkit
"
RDEPEND="${DEPEND}"
BDEPEND="
	dev-util/nvidia-cuda-toolkit
"

src_prepare() {
	cmake_src_prepare

	# Running into a ebuild bug where this doesn't get run causing
	# cuda to use an incompatible GCC compiler
	cuda_src_prepare
}

src_configure() {
	# Doesn't like LTO
	filter-flags -flto*

	# Force a GCC binary that CUDA supports (pulled from tensorflow)
	#
	# I'd like to find a ccache friendly solution, but I guess this
	# will have to do.
	export GCC_HOST_COMPILER_PATH="$(cuda_gccdir)/$(tc-getCC)"
	export CC="$(cuda_gccdir)/$(tc-getCC)"
	export CXX="$(cuda_gccdir)/$(tc-getCXX)"

	cmake_src_configure
}

src_compile() {
	# We only need the shared object
	cmake_src_compile libxmrig-cuda.so
}

src_install() {
	# It appears that there's no install target.
	dolib.so "${BUILD_DIR}/libxmrig-cuda.so"
}

pkg_postinst() {
	einfo
	einfo "You should be able to enable CUDA for xmrig by"
	einfo "adding this to config.json:"
	einfo
	einfo ' "cuda": {'
	einfo '     "enable": true'
	einfo ' }'
	einfo
}
