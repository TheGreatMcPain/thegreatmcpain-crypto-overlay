# TheGreatMcPain's Cryptocurrency Gentoo Overlay

This overlay contains Crypto related ebuilds that were originally in my main Gentoo overlay.

## The Crypto Currencies supported via this overlay are.

- Monero (XMR) - `net-p2p/monero`
- Chia (XCH) - `net-p2p/chia-blockchain`
- HavenProtocal (XHV) - `net-p2p/haven`
- TurtleCoin (TRTL) - `net-p2p/turtlecoin`
- Masari (MSR) - `net-p2p/masari`

## Installation

### Layman

```
# layman -o https://gitlab.com/TheGreatMcPain/thegreatmcpain-crypto-overlay/-/raw/master/repository.xml -f -a thegreatmcpain-crypto
```

### Manual

Add this to '/etc/portage/repos.conf/thegreatmcpain-crypto.conf'

```
[thegreatmcpain-crypto]
sync-uri = https://gitlab.com/TheGreatMcPain/thegreatmcpain-crypto-overlay.git
sync-type = git
location = /var/db/repos/thegreatmcpain-crypto
```

## Contributing

The repository is mirrored from gitlab to github.\
Even though I prefer to use my Gitlab I will most-likely accept pull-requests, and issues, on Github.

I highly recommend the use of `repoman` for creating commits, because it'll usually catch errors before commiting (such as an outdated Manifest).

To create a commit run `repoman commit`.

If for some reason you can't use repoman please use the following commit format.

```
category/package: the thing I did

More information about the thing I did.
```
